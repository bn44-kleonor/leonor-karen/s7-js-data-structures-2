/*============================================
ACTIVITY

MODULE: WD004-S6-DATA-STRUCTURES-1
GitLab: s6-js-data-structures-1

Challenge:
Swap adjacent (i.e., magkasunod) elements in an arrray if they are in wrong order so 
that the original array will contain the elements/values 1,3,6,9.
Do not use sort() method.


Steps:
1.a. Initialize an array called "arr" with numbers 6, 9, 1, and 3 as its values/elements.
1.b. Declare a variable called tempElement.
1.c. Display arr in the console:
Before: 6,9,1,3


2. Iterate over all the elements in arr so that you can display the ff in your console:
  Compare 6 and 9
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 1
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 3
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 1
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 3
  (4)&nbsp;[6, 9, 1, 3]
  Compare 1 and 3

3. Create a condition so that when the first element in the comparison is greater than the second element, "Swap!" will be displayed. Your console should now display the following:
  Compare 6 and 9
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 1
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 3
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 1
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 3
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 1 and 3
  (4)&nbsp;[6, 9, 1, 3]


4. Inside your condition, write a code that will swap the values of the first and second element so that your console would now look like this:
  Compare 6 and 9
  (4)&nbsp;[6,9,1,3]
  Compare 6 and 1
  Swap!
  (4) [1,9,6,3]
  Compare 1 and 3
  (4)&nbsp;[1,9,6,3]
  Compare 9 and 6
  Swap!
  (4)&nbsp;[1,6,9,3]
  Compare 6 and 3
  Swap!
  (4)&nbsp;[1,3,9,6]
  Compare 9 and 6
  Swap!
  (4)&nbsp;[1,3,6,9]

  CLUE: You were asked to declare a tempElement variable earlier. You will be able to use it here to store something... :)

5. Display arr in the console:
After: 1,3,6,9

Stretch Goal:
Enclose your code inside a function and fetch your arr values via prompt.

===============================================*/

//1
/*1.a. Initialize an array called "arr" with numbers 6, 9, 1, and 3 as its values/elements.
1.b. Declare a variable called tempElement.
1.c. Display arr in the console:
Before: 6,9,1,3  */
// console.log("step 1 ***********************************************")
// let arr = [6, 9, 1, 3];
// let tempElement;

// for(let i=0; i<arr.length; i++) {
//   console.log(arr[i]);
// }

// //2  (match not repeated combinations/positions)
// console.log("step 2 ***********************************************")
// for(let x=0; x<=arr.length-1; x++) {
//   for(let y=x+1; y <=arr.length-1; y++) {
//     console.log(arr[x], arr[y]);  
//   }
// }


// //3
// console.log("step 3 ***********************************************")
// for(let x=0; x<=arr.length-1; x++) {
//   for(let y=x+1; y <=arr.length-1; y++) {
//     console.log(arr[x], arr[y]);
//     if(arr[x] > arr[y]) {
//       console.log("Switch!");
//     }
//   }
// }


// //4
// console.log("step 4 ***********************************************")
// for(let x=0; x<=arr.length-1; x++) {
//   for(let y=x+1; y <=arr.length-1; y++) {
//     console.log(arr[x], arr[y]);
//     if(arr[x] > arr[y]) {
//       console.log("Switch!");
//       tempElement = arr[y];
//       arr[y] = arr[x];
//       arr[x] = tempElement;
//     }
//   }
// }


// // 5. Display arr in the console:
// // After: 1,3,6,9
// console.log("After " + arr);



// streched goal:
console.log("streched goal ***********************************************");
arr = prompt("Enter numbers to be sorted. Add comma in between them.");
console.log('Before ' + arr);


function sortArray(arr) {
  let tempElement;

  arr = arr.split(",");
  for(let x=0; x<=arr.length-1; x++) {
    for(let y=x+1; y <=arr.length-1; y++) {
      console.log(arr[x], arr[y]);
      if(arr[x] > arr[y]) {
        console.log("Switch!");
        tempElement = arr[y];
        arr[y] = arr[x];
        arr[x] = tempElement;
      }
      console.log(arr);
    }
  }
  //console.log(`After: ${arr}`);
  return arr;
}

console.log("After :" + sortArray(arr));

//sortArray(arr);
