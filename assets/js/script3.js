/*========================
DISCUSSION

Module: WD004-S7-DATA-STRUCTURES-2


JS OBJECTS
If array is an indexed collection of data, objects are keyed collection 
of data (aka properties).

=========================*/
const dog = {
	name: 'Bruno',
	breed: 'Boxer'
}

console.log(dog);

//We ACCESS values through their respective KEYS using dog or bracket notation
console.log("********ACCESS*****KEYS********")
console.log(dog.name);		// benefit: easy to type
console.log(dog['breed']);	// benefit: value inside bracket notation can be a variable
	let property = 'breed';
	console.log(dog[property]);	//dynamically fetch properties


//We ADD & UPDATE PROPERTIES via dot and bracket notation
console.log("********ADD & UPDATE PROPERTIES*********")
dog.name = "Harry";
dog.breed = 'Bulldog';
dog.age = 2; 		//adding property 'age'
dog['age'] = 3;
console.log(dog);

/*
Objects defined using ___LITERAL NOTATION___ are called ___SINGLETON___. 
This means changes made to the object affects that object across the entire script. 
*/
console.log("*****************")
const dog2 = {
	name: 'Max',
	breed: 'Beagle'
}

const dog3 = {
	name: 'Matilda',
	breed: 'Poodle'
}


/*
Object defined with a ___CONSTRUCTOR FUNCTION___ let us have multiple instances of that object.
STEPS:
1. Define the object type by writing a constructor function. Use capital initial letter.
2. Create an instance (instantiation) of the object with the keyword ___"new"___.
*/
console.log("*****************")

function Dog(name, breed) {			// starts in Big letter
	this.name = name,				//'this'
	this.breed = breed
}

let dogA = new Dog("Bubbles", "Aspin");
let dogB = new Dog("Happy", "Chihuahua");
let dogC = new Dog("Bobo", "Pug");
console.log(dogA);
console.log(dogB);
console.log(dogC);
console.log("*****************")	//kung alin lang instance dinagdagan mo ng property, yun lang ang magkakaroon ng ganung property (unlike SINGLETON)
dogA.age = 3;
dogB.color = "Brown";
console.log(dogA);
console.log(dogB);
console.log(dogC);
console.log("*****************")
dogA.age = 3;
dogB.color = "Brown";
delete dogC.name;
delete dogC["name"];
console.log(dogA);
console.log(dogB);
console.log(dogC);
console.log(dogC.name);

/*
Unlike singletons, changes to objects created with a constructor function does not affect their instances.
*/
console.log("*****************")


/*=====================================
PRACTICE 

On your own, create a student constructor function with name, level, and age properties
Instantiate three student objects and assign them to the variables student1, student2, and student3
=======================================*/

console.log("*** PRACTICE **************")

function Student(name, level, age) {
	this.name = name,
	this.level = level,
	this.age = age
}

let student1 = new Student("Name1", "Level1", "7");
let student2 = new Student("Name2", "Level2", "8");
let student3 = new Student("Name3", "Level3", "9");
console.log(student1);
console.log(student2);
console.log(student3);

