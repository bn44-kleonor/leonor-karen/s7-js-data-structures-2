/*================================ 
GUIDED PRACTICE 
Module: WD004-S7-DATA-STRUCTURES-2 
QUEUE 
A linear data structure that follows the mechanism: FIRST IN, FIRST OUT 
===================================*/ 


//STEP 1: Define stack constructor function

function Queue(){
}

//let q = new Queue();


//STEP2: Create the collection property which an initial value of an empty array

function Queue(){
	let collection = [];
}
//let q = new Queue();


//STEP3: Create the following methods. Instantiate the queue object to test it along the way.

//a) print - prints the collection in the console
//b) enqueue - adds an element to the end of the queue 
//c) dequeue - removes the first item in the collection and RETURNS its value 
//d) front - RETURNS the first element of the queue 
//e) size - RETURNS the number of elements in the queue 
//f) isEmpty - checks if the queue is empty or not and RETURNS the result

function Queue(){
	let collection = [];

	//print - prints the collection in the console
	this.print = function() {				//method is function without name because it will not be called by its name
		console.log(collection);
	}

	//b) enqueue - adds an element to the end of the queue 
	this.enqueue = function(element) {
		collection.push(element);
	}

	//dequeue - removes the first item in the collection and RETURNS its value 
	this.dequeue = function() {
		return collection.shift();		//don't have to pass value because it will always remove the 'first' item in the collection
	}

	//front - RETURNS the first element of the queue 
	this.front = function() {
		return collection[0];
	}

	//size - RETURNS the number of elements in the queue 
	this.size = function() {
		return collection.length;
	}

	//isEmpty - checks if the queue is empty or not and RETURNS the result
	this.isEmpty = function() {
		return collection.length === 0;
	}


}


let q = new Queue();
q.print();
q.enqueue(1);
q.enqueue(2);
q.enqueue(3);
q.print();
console.log(q.dequeue());
q.print();
console.log(q.front());
console.log(q.size());
console.log(q.isEmpty());
