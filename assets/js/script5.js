/*================================ 

ACTIVITY

Module: WD004-S7-DATA-STRUCTURES-2
GitLab: s7-data-structures-2

STACK 
A linear data structure that follows the mechanism: LAST IN, FIRST OUT.
===================================*/

//STEP 1: Define stack constructor function

//STEP2: Create the following properties
//count with the initial value of 0
//storage - an empty object

//STEP3: Create the following methods. Instantiate the stack object to test it along the way.
//a) push - adds a value at the end of the stack (i.e.,storage property).
//CLUE: update value of count whenever you add a value at the end of the stack.

//b) pop - removes the value at the end of the stack and RETURNS the removed value
//CLUE 2: check value of count in relation to property. 

//c) length - REETURNS the number of values stored in the stack 
//d) peek - RETURNS the value at the top of the stack

//NOTE: storage is an object, not an array.


function Stack(){
	this.count = 0;
	this.storage = {};

	//print - prints the collection in the console
	this.print = function() {				//method is function without name because it will not be called by its name
		console.log(this.storage);
	}

	//push
	//CLUE: update value of count whenever you add a value at the end of the stack.
	this.push = function(element) {
		this.storage[this.count] = element;
		this.count++;
	}

	//pop - removes the value at the end of the stack and RETURNS the removed value
	//CLUE 2: check value of count in relation to property. 
	this.pop = function(element) {
		console.log("*******pop*******");
		//console.log("pop element of this.count value =  " + this.count);
		let tempElement = this.storage[this.count-1];  //use this because if I delete it already without remembering it, i will not be able to return the deleted value since it was deleted already.
		delete this.storage[this.count-1];	// delete should come 'first' because if i 'return' already it will not execute the next step.
		return tempElement;
	}

	//length - REETURNS the number of values stored in the stack 
	this.length = function() {
	console.log("*******length*******");
		//console.log("pop this.count = " + this.count);
		return this.count-1;
	}

	//peek
	this.peek = function() {
	console.log("*******peek*******");
		return this.storage[0];
	}
}

let newStack = new Stack();

newStack.print();
newStack.push(34);
newStack.push(2);
newStack.push(3);
newStack.push(7);
newStack.push(90);
newStack.push(20);
console.log(newStack.storage);
console.log("Value removed = " + newStack.pop());
console.log(newStack.storage);
console.log("Length value = " + newStack.length());
console.log(newStack.storage);
console.log("Peek value = " + newStack.peek());
console.log(newStack.storage);


//console.log(newStack.push(1));
//console.log(newStack.push(2));
//console.log(newStack.push(3));
//console.log(newStack.storage);

//newStack.push(1);


