/*============================================
ACTIVITY

MODULE: WD004-S6-DATA-STRUCTURES-1
GitLab: s6-js-data-structures-1

Challenge:
Swap adjacent (i.e., magkasunod) elements in an arrray if they are in wrong order so 
that the original array will contain the elements/values 1,3,6,9.
Do not use sort() method.


Steps:
1.a. Initialize an array called "arr" with numbers 6, 9, 1, and 3 as its values/elements.
1.b. Declare a variable called tempElement.
1.c. Display arr in the console:
Before: 6,9,1,3


2. Iterate over all the elements in arr so that you can display the ff in your console:
  Compare 6 and 9
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 1
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 3
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 1
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 3
  (4)&nbsp;[6, 9, 1, 3]
  Compare 1 and 3

3. Create a condition so that when the first element in the comparison is greater than the second element, "Swap!" will be displayed. Your console should now display the following:
  Compare 6 and 9
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 1
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 6 and 3
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 1
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 9 and 3
  Swap!
  (4)&nbsp;[6, 9, 1, 3]
  Compare 1 and 3
  (4)&nbsp;[6, 9, 1, 3]


4. Inside your condition, write a code that will swap the values of the first and second element so that your console would now look like this:
  Compare 6 and 9
  (4)&nbsp;[6,9,1,3]
  Compare 6 and 1
  Swap!
  (4) [1,9,6,3]
  Compare 1 and 3
  (4)&nbsp;[1,9,6,3]
  Compare 9 and 6
  Swap!
  (4)&nbsp;[1,6,9,3]
  Compare 6 and 3
  Swap!
  (4)&nbsp;[1,3,9,6]
  Compare 9 and 6
  Swap!
  (4)&nbsp;[1,3,6,9]

  CLUE: You were asked to declare a tempElement variable earlier. You will be able to use it here to store something... :)

5. Display arr in the console:
After: 1,3,6,9

Stretch Goal:
Enclose your code inside a function and fetch your arr values via prompt.

===============================================*/

//1 and 2
// let zodiacSigns = [ 
//   "",
//   {
//     month: "january",
//     border: 20,
//     signs: [ "Aquarius", "Capricorn"]
//   },
//   {
//     month: "february",
//     border: 19,
//     signs: [ "Pisces", "Aquarius"]
//   },
//   {
//     month: "march",
//     border: 21,
//     signs: [ "Aries", "Pisces"]
//   },
//   {
//     month: "april",
//     border: 20,
//     signs: [ "Taurus", "Aries"]
//   },
//   {
//     month: "may",
//     border: 21,
//     signs: [ "Gemini", "Taurus"]
//   },
//   {
//     month: "june",
//     border: 21,
//     signs: [ "Cancer", "Gemini"]
//   },
//   {
//     month: "july",
//     border: 23,
//     signs: [ "Leo", "Cancer"]
//   },
//   {
//     month: "august",
//     border: 23,
//     signs: [ "Virgo", "Leo"]
//   },
//   {
//     month: "september",
//     border: 23,
//     signs: [ "Libra", "Virgo"]
//   },
//   {
//     month: "october",
//     border: 23,
//     signs: [ "Scorpio", "Libra"]
//   },
//   {
//     month: "november",
//     border: 22,
//     signs: [ "Saggitarius", "Scorpio"]
//   },
//   {
//     month: "december",
//     border: 22,
//     signs: [ "Capricorn", "Saggitarius"]
//   }
// ]


//3
let monthInput = prompt("What is your birth month (number)?");
let dayInput = prompt ("What is your birth 'date' (day of the month)?");

// //4
// // kaya ganito ka-ili kasi number dapat ang input "hindi word" - magbasa
// console.log(zodiacSigns[1]);
// function checkZodiac(monthInput, dayInput) {
//   //console.log(typeof dayInput);
//   //console.log(typeof border);
//   let day = parseInt(dayInput);
//   let border = zodiacSigns[monthInput].border;
//   if (day >= border){
//     console.log(`Your zodiac sign is ${zodiacSigns[monthInput].signs[1]}`);
//   } else {
//     console.log(`Your zodiac sign is ${zodiacSigns[monthInput].signs[0]}`);
//   }
// }

// checkZodiac(monthInput, dayInput);


function monthInNumbers(monthInput) {
  switch(monthInput.toLowerCase()) {
    case 'january':
      monthInput = 1;
      break;
    case 'february':
      monthInput = 2;
      break;
    case 'march':
      monthInput = 3;
      break;
    case 'april':
      monthInput = 4;
      break;
    case 'may':
      monthInput = 5;
      break;
    case 'june':
      monthInput = 6;
      break;
    case 'july':
      monthInput = 7;
      break;
    case 'august':
      monthInput = 8;
      break;
    case 'september':
      monthInput = 9;
      break;
    case 'october':
      monthInput = 10;
      break;
    case 'november':
      monthInput = 11;
      break;
    case 'december':
      monthInput = 12;
      break;
  }
  return monthInput;
}

// function checkZodiac(monthInput, dayInput) {

//   let monthInNum = monthInNumbers(monthInput);
//   //console.log(monthInNum);

//   let day = parseInt(dayInput);
//   let border = zodiacSigns[monthInNum].border;      //because monthInput is not a word then change monthInput (in variables) to monthInNum
//   monthInput = monthInput.toUpperCase();
//   if(monthInNum !== 0 ) {
//     if (day >= border){
//       console.log(`You were born in ${monthInput} ${dayInput}. Your zodiac sign is ${zodiacSigns[monthInNum].signs[1]}`);
//     } else {
//       console.log(`You were born in ${monthInput} ${dayInput}. Your zodiac sign is ${zodiacSigns[monthInNum].signs[0]}`);
//     }
//   } else {
//     console.log(`Sorry Your zodiac sign for ${monthInput} ${dayInput} doesn't exist.`)
//   }
// }

// checkZodiac(monthInput, dayInput);



//streched goal 3

zodiacSigns = [ "",
                {
                  month: "January",
                  border: 20,
                  signs: [  {name:"Aquarius",
                            horoscope: function(){
                                console.log(this.name);
                                return 'Deep and Creative ' + this.name; //value of the name property
                                //Deep and Creative Aquarius
                                }
                            },
                            {name:"Capricorn",
                            horoscope: function(){
                                return 'Serious and Strong ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "February",
                  border: 19,
                  signs: [  {name:"Pisces",
                            horoscope: function(){
                                return 'Wise and Artistic ' + this.name;
                                }
                            },
                            {name:"Aquarius",
                            horoscope: function(){
                                return 'Deep and Creative ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "March",
                  border: 21,
                  signs: [  {name:"Aries",
                            horoscope: function(){
                                return 'Eager and Quick ' + this.name;
                                }
                            },
                            {name:"Pisces",
                            horoscope: function(){
                                return 'Wise and Artistic ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "April",
                  border: 20,
                  signs: [  {name:"Taurus",
                            horoscope: function(){
                                return 'Strong and Reliable ' + this.name;
                                }
                            },
                            {name:"Aries",
                            horoscope: function(){
                                return 'Eager and Quick ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "May",
                  border: 21,
                  signs: [  {name:"Gemini",
                            horoscope: function(){
                                return 'Curious and Kind ' + this.name;
                                }
                            },
                            {name:"Taurus",
                            horoscope: function(){
                                return 'Strong and Reliable ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "June",
                  border: 21,
                  signs: [  {name:"Cancer",
                            horoscope: function(){
                                return 'Sensitive and Loyal ' + this.name;
                                }
                            },
                            {name:"Gemini",
                            horoscope: function(){
                                return 'Curious and Kind ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "July",
                  border: 23,
                  signs: [  {name:"Leo",
                            horoscope: function(){
                                return 'Fiery and Confident ' + this.name;
                                }
                            },
                            {name:"Cancer",
                            horoscope: function(){
                                return 'Sensitive and Loyal ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "August",
                  border: 23,
                  signs: [  {name:"Virgo",
                            horoscope: function(){
                                return 'Gentle and Smart ' + this.name;
                                }
                            },
                            {name:"Leo",
                            horoscope: function(){
                                return 'Fiery and Confident ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "September",
                  border: 23,
                  signs: [  {name:"Libra",
                            horoscope: function(){
                                return 'Sociable and Fair ' + this.name;
                                }
                            },
                            {name:"Virgo",
                            horoscope: function(){
                                return 'Gentle and Smart ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "October",
                  border: 23,
                  signs: [  {name:"Scorpio",
                            horoscope: function(){
                                return 'Original and Brave ' + this.name;
                                }
                            },
                            {name:"Libra",
                            horoscope: function(){
                                return 'Sociable and Fair ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "November",
                  border: 22,
                  signs: [  {name:"Saggitarius",
                            horoscope: function(){
                                return 'Funny and Generous ' + this.name;
                                }
                            },
                            {name:"Scorpio",
                            horoscope: function(){
                                return 'Original and Brave ' + this.name;
                                }
                            }
                          ]
                },
                {
                  month: "December",
                  border: 22,
                  signs: [  {name:"Capricorn",
                            horoscope: function(){
                                return 'Serious and Strong ' + this.name;
                                }
                            },
                            {name:"Saggitarius",
                            horoscope: function(){
                                return 'Funny and Generous ' + this.name;
                                }
                            }
                          ]
                }
]




function checkZodiac(monthInput, dayInput) {

  let monthInNum = monthInNumbers(monthInput);
  //console.log(monthInNum);

  let day = parseInt(dayInput);
  let border = zodiacSigns[monthInNum].border;      //because monthInput is not a word then change monthInput (in variables) to monthInNum
  monthInput = monthInput.toUpperCase();

  if(monthInNum !== 0 ) {
    if (day >= border){
      console.log(`You were born in ${monthInput} ${dayInput}. Your zodiac sign is ${zodiacSigns[monthInNum].signs[1].name}`);
      console.log("Your horoscope is " + zodiacSigns[1].signs[1].horoscope());
    } else {
      console.log(`You were born in ${monthInput} ${dayInput}. Your zodiac sign is ${zodiacSigns[monthInNum].signs[0].name}`);
      console.log("Your horoscope is " + zodiacSigns[1].signs[1].horoscope());
    }
  } else {
    console.log(`Sorry Your zodiac sign for ${monthInput} ${dayInput} doesn't exist.`)
  }
}

checkZodiac(monthInput, dayInput);




